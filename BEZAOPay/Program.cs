﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAOPayDAL;
using BEZAOPayDAL.Models;
using System.Data.SqlClient;



namespace BEZAOPay
{
    class Program
    {
        static void Main(string[] args)
        {
            var db = new BEZAODAL();

            db.GetAllUsers();

            //Insert a new User with Procedure
            //db.InsertUserProcedure("Ezra", "A.Ezar@domain.com");

            //Insert new account with Procedure
            //db.InsertAccountProcedure(20, 45236789, (decimal)2400000.99);

            //deleting a user
            //db.DeleteUser(8);

            //Updating a user
            //db.UpdateUser(16, "Francis@domain.com");


            //Deleting and inserting using Transactions
            var user = new User
            {

                Name = "Chinedu",
                Email = "Chinedu.M@domain.com"
            };

            //db.UsingTransactions(17, user);
        }
    }
}
