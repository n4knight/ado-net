﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BEZAOPayDAL.Models;

namespace BEZAOPayDAL
{
   public class BEZAODAL
   {
        private readonly string _connectionString;
        private SqlConnection _sqlConnection;

        public BEZAODAL() :
            this(@"Data source=(localdb)\mssqllocaldb; integrated security=true; initial catalog=BEZAOPay")
        {

        }

        public BEZAODAL(string connectionString)
        {
            _connectionString = connectionString;
        }

        private void OpenConnection()
        {
            _sqlConnection = new SqlConnection { ConnectionString = _connectionString };
            _sqlConnection.StateChange += (sender, e) =>
            {
                Console.WriteLine($"db state changed from {e.OriginalState} to {e.CurrentState}");
            };
            _sqlConnection.Open();

            Console.WriteLine();
        }

        private void CloseConnection()
        {
            if (_sqlConnection?.State != ConnectionState.Closed)
                _sqlConnection?.Close();

            Console.WriteLine();
        }


        public void GetAllUsers()
        {
            OpenConnection();

            var query = @"SELECT * FROM USERS";

            using (var command = new SqlCommand(query, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                Console.WriteLine("All users in the db");
                while (reader.Read())
                {
                    Console.WriteLine($"-> Name: {reader["Name"]}, Email: {reader["Email"]}");
                }
                reader.Close();
            }

            CloseConnection();


        }

        public void DeleteUser(int id)
        {
            OpenConnection();

            string sql = $"DELETE FROM USERS WHERE Id = {@id}";

            using (var command = new SqlCommand(sql, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                try
                {
                    command.Parameters.AddWithValue("id", id);
                    int record = command.ExecuteNonQuery();
                    Console.WriteLine($"{record} record deleted");

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }

            CloseConnection();
        }

        public void UpdateUser(int id, string newEmail)
        {
            OpenConnection();

            string sql = $"UPDATE USERS SET Email = '{newEmail}' where Id = '{id}'";
            using (var command = new SqlCommand(sql, _sqlConnection))
            {
                command.CommandType = CommandType.Text;

                try
                {
                    command.Parameters.AddWithValue("Id", id);
                    command.Parameters.AddWithValue("Email", newEmail);
                    int recordUpdated = command.ExecuteNonQuery();

                    Console.WriteLine($"{recordUpdated} record updated to {command.Parameters["Email"].Value}");

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            CloseConnection();
        }

        public void UsingTransactions(int Id, User user)
        {
            OpenConnection();

            var tx = _sqlConnection.BeginTransaction();
            string sqlDltCmd = $"DELETE FROM USERS WHERE Id = '{@Id}'";
            string sqlInstCmd = $"INSERT INTO USERS (Name, Email) VALUES ('{@user.Name}', '{@user.Email}')";

            try
            {

                using (var command = new SqlCommand(sqlDltCmd, _sqlConnection, tx))
                {
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue("Id", Id);
                    int recordsDeleted = command.ExecuteNonQuery();
                    Console.WriteLine($"{recordsDeleted} record(s) deleted");
                }

                using (var command = new SqlCommand(sqlInstCmd, _sqlConnection, tx))
                {
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue("Name", user.Name);
                    command.Parameters.AddWithValue("Email", user.Email);
                    int recordsAdded = command.ExecuteNonQuery();
                    Console.WriteLine($"{recordsAdded} record(s) Added");
                }

                tx.Commit();

            }
            catch (Exception ex)
            {

                Console.WriteLine($"{ex.Message}. Rolling Back.");
                tx.Rollback();
            }

            CloseConnection();

        }

        public void InsertUserProcedure(string name, string email)
        {
            OpenConnection();

            using (var command = new SqlCommand("INSERTUSER", _sqlConnection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("Name", name);
                command.Parameters.AddWithValue("Email", email);
                int RecordsAdded = command.ExecuteNonQuery();

                Console.WriteLine($"{RecordsAdded} Records added to Users Table");

            }

            CloseConnection();
        }

        public void InsertAccountProcedure(int userId, int accountNumber, decimal balance)
        {
            OpenConnection();

            using (var command = new SqlCommand("INSERTACCOUNT", _sqlConnection))
            {

                command.CommandType = CommandType.StoredProcedure;

                var sqlParameter = new SqlParameter
                {
                    ParameterName = "userId",
                    SqlDbType = SqlDbType.Int,
                    Value = userId,
                    Direction = ParameterDirection.Input
                };
                command.Parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter
                {
                    ParameterName = "accountNumber",
                    SqlDbType = SqlDbType.Int,
                    Value = accountNumber,
                    Direction = ParameterDirection.Input

                };
                command.Parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter
                {
                    ParameterName = "balance",
                    SqlDbType = SqlDbType.Decimal,
                    Value = balance,
                    Direction = ParameterDirection.Input
                };
                command.Parameters.Add(sqlParameter);
                int recordsAdded = command.ExecuteNonQuery();

                Console.WriteLine($"{recordsAdded} records added to Accounts Table");
            }

            CloseConnection();
        }
    }
}
