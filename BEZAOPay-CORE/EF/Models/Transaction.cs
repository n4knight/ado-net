﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BEZAOPay_CORE.EF.Models
{
    public class Transaction
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        [Required]
        [StringLength(10)]
        public string Mode { get; set; }

        public decimal Amount { get; set; }

        public DateTime Time { get; set; }

        public virtual User User { get; set; }
    }
}
