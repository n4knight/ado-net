﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAOPay_CORE.EF.Models;

namespace BEZAOPay_CORE.EF.Models
{
    public class User
    {
        public User()
        {
            Accounts = new List<Account>();
            Transactions = new List<Transaction>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public virtual List<Account> Accounts { get; set; }

        public virtual List<Transaction> Transactions { get; set; }


    }

}
