﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BEZAOPay_CORE.EF.Models
{
    public class Account
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int Account_Number { get; set; }

        public double Balance { get; set; }

        public virtual User User { get; set; }
    }
}
