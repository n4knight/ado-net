﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAOPay_CORE.EF.Models;
using Microsoft.EntityFrameworkCore;

namespace BEZAOPay_CORE.EF
{
    class BEZAOPayContext : DbContext
    {

        //public BEZAOPayContext(DbContextOptions<BEZAOPayContext> options)
        //    :base(options)
        //{

        //}

        const string connectionString = @"server=(localdb)\mssqllocaldb; 
                database=BEZAOPayy; trusted_Connection=true";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(connectionString);
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("BZ");

            modelBuilder.Entity<Account>()
                .ToTable("Accounts")
                .HasKey(a => a.Id);

            modelBuilder.Entity<Account>().Property(a => a.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<User>()
                .ToTable("Users")
                .HasKey(u => u.Id);

            modelBuilder.Entity<Transaction>()
                .ToTable("Transactions")
                .HasKey(t => t.Id);

            modelBuilder.Entity<Transaction>().Property(t => t.Id)
                .ValueGeneratedOnAdd();


            //Relationships - they suck
            modelBuilder.Entity<User>()
                .HasMany(a => a.Accounts)
                .WithOne(u => u.User)
                .HasForeignKey(a => a.Id);

            modelBuilder.Entity<User>()
                .HasMany(t => t.Transactions)
                .WithOne(u => u.User)
                .HasForeignKey(t => t.Id);

            modelBuilder.Entity<Account>()
                .HasOne(u => u.User)
                .WithMany(a => a.Accounts)
                .HasForeignKey(a => a.UserId);


            modelBuilder.Entity<Transaction>()
                .HasOne(u => u.User)
                .WithMany(t => t.Transactions)
                .HasForeignKey(u => u.UserId);
        }
    }
}
