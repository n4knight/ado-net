﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAOPay_CORE.EF.Models;
using Microsoft.EntityFrameworkCore.Storage;

namespace BEZAOPay_CORE.EF
{
    public static class DbOperations
    {
        //private static BEZAOPayContext _bezaoContext;

        //public DbOperations(BEZAOPayContext bezaoContext)
        //{
        //    _bezaoContext = bezaoContext;
        //}
        public static async Task createDatabase()
        {
            using (var contxt = new BEZAOPayContext())
            {
                bool created = await contxt.Database.EnsureCreatedAsync();
                string createdInfo = created ? "Created" : "Exist";
                Console.WriteLine($"Database {createdInfo}");
            }
            Console.WriteLine();
        }

        public static async Task AddUser()
        {
            using (var contxt = new BEZAOPayContext())
            {


                User user = new User()
                {
                    Name = "Messi",
                    Email = "Messi@psg.com",
                };

                await contxt.Users.AddAsync(user);
                int record = await contxt.SaveChangesAsync();

                Console.WriteLine($"{record} records with name: {user.Name} added to Users Table");
            }
            Console.WriteLine();
        }

        public static async Task AddNewAccount()
        {
            using (var contxt = new BEZAOPayContext())
            {
                await contxt.Accounts.AddAsync(new Account()
                {

                    Account_Number = 0087324287,
                    Balance = 370000.98
                });

                int record = await contxt.SaveChangesAsync();
                Console.WriteLine($"{record} records added to Accounts Table");
            }
            Console.WriteLine();
        }

        public static async Task AddNewTransaction()
        {
            using (var contxt = new BEZAOPayContext())
            {
                await contxt.Transactions.AddAsync(new Transaction()
                {

                    Amount = 370000,
                    Mode = "Debit",
                    Time = DateTime.Now
                }); ;

                int records = await contxt.SaveChangesAsync();
                Console.WriteLine($"{records} record added to Transaction");
            }
            Console.WriteLine();
        }

        public static async Task InsertUserAsync(User userEntity,
            Account accountEntity, Transaction transactionEntity)
        {
            IDbContextTransaction tx = null;
            try
            {
                using (var contxt = new BEZAOPayContext())
                using (tx = contxt.Database.BeginTransaction())
                {
                    var user = new User
                    {
                        Name = userEntity.Name,
                        Email = userEntity.Email
                    };

                    contxt.Users.Add(user);
                    //int record = await contxt.SaveChangesAsync();
                    //Console.WriteLine("User added");

                    var account = new Account
                    {
                        UserId = user.Id,
                        Account_Number = accountEntity.Account_Number,
                        Balance = accountEntity.Balance,
                    };
                    var transaction = new Transaction()
                    {
                        UserId = user.Id,
                        Amount = transactionEntity.Amount,
                        Time = transactionEntity.Time,
                        Mode = transactionEntity.Mode
                    };

                    user.Transactions.Add(transaction);
                    user.Accounts.Add(account);

                    await contxt.Transactions.AddAsync(transaction);
                    await contxt.Accounts.AddAsync(account);

                    await contxt.SaveChangesAsync();
                    Console.WriteLine("User added");

                    await tx.CommitAsync();

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                await tx.RollbackAsync();
            }

        }

        public static void GetAllUsers()
        {
            try
            {
                using (var contxt = new BEZAOPayContext())
                {
                    var users = contxt.Users.ToList();
                    if (users != null)
                    {
                        foreach (var user in users)
                        {
                            Console.WriteLine($"Id -> {user.Id}, Name -> {user.Name}");
                        }
                    }

                }
            }
            catch (ArgumentNullException ae)
            {
                Console.WriteLine(ae.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine();
        }

        public static void GetAllAccounts()
        {
            try
            {
                using (var contxt = new BEZAOPayContext())
                {
                    var accounts = contxt.Accounts.ToList();
                    if (accounts != null)
                    {
                        foreach (var account in accounts)
                        {
                            contxt.Entry(account).Reference(u => u.User).Load();
                            Console.WriteLine($"Account Owner: {account.User.Name}");
                            Console.WriteLine($"Account Number: {account.Account_Number}");
                            Console.WriteLine($"Account Balance: {account.Balance}");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine();
        }

        public static void GetAllTransactions()
        {
            try
            {
                using (var contxt = new BEZAOPayContext())
                {
                    var transactions = contxt.Transactions.ToList();
                    if (transactions != null)
                    {
                        foreach (var transaction in transactions)
                        {
                            contxt.Entry(transaction).Reference(u => u.User).Load();
                            Console.WriteLine($"Transactions By: {transaction.User.Name}");
                            Console.WriteLine($"Transaction Mode: {transaction.Mode}");
                            Console.WriteLine($"Account Balance: {transaction.Time}");
                            Console.WriteLine($"Account Amount: {transaction.Amount}");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine();
        }

        public async static Task GetUser(string name)
        {
            try
            {
                using (var contxt = new BEZAOPayContext())
                {
                    var user = contxt.Users.Find(UserIdFinder(name));
                    if (user is not null)
                    {
                        await contxt.Entry(user).Collection(u => u.Accounts).LoadAsync();
                        await contxt.Entry(user).Collection(u => u.Transactions).LoadAsync();
                        Console.WriteLine($"user Datails-> Name: {user.Name} " +
                            $" Email: {user.Email}. With\n");
                        Console.WriteLine($"{user.Accounts.Count} accounts and\n");
                        Console.WriteLine($"{user.Transactions.Count} transactions");
                        return;
                    }

                    throw new Exception($"No User with Name {name} found");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            int UserIdFinder(string name)
            {
                using (var cntxt = new BEZAOPayContext())
                {
                    foreach (var user in cntxt.Users)
                    {
                        if (user.Name == name)
                            return user.Id;
                    }
                }

                return 0;
            }
        }

        public static async Task DeleteDatabase()
        {
            Console.WriteLine("Delete Database: y/n");
            var input = Console.ReadLine();

            if (input.ToLower() == "y")
            {
                using (var contxt = new BEZAOPayContext())
                {
                    bool deleted = await contxt.Database.EnsureDeletedAsync();
                    string deleteInfo = deleted ? "Deleted" : "Not Deleted";
                    Console.WriteLine($"Database {deleteInfo}");
                }
            }
            Console.WriteLine();
        }

    }
}
