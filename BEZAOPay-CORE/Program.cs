﻿using System;
using BEZAOPay_CORE.EF;
using System.Threading;
using System.Threading.Tasks;
using BEZAOPay_CORE.EF.Models;

namespace BEZAOPay_CORE
{
    class Program
    {
        static async Task Main(string[] args)
        {
            try
            {
                var user = new User
                {
                    Name = "Messi",
                    Email = "messi@Psg.com"
                };

                var account = new Account
                {
                    Account_Number = 0087324287,
                    Balance = 2300000
                };

                var transaction = new Transaction
                {
                    Amount = 2300000,
                    Mode = "DEPOSIT",
                    Time = DateTime.Now
                };

                await DbOperations.DeleteDatabase();
                await DbOperations.createDatabase();

                //await DbOperations.AddUser();
                //await DbOperations.AddNewAccount();
                //await DbOperations.AddNewTransaction();

                await DbOperations.InsertUserAsync(user, account, transaction);
                await DbOperations.GetUser(user.Name);
                //DbOperations.GetAllUsers();
                //DbOperations.GetAllAccounts();
                //DbOperations.GetAllTransactions();




            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException?.Message);
            }
        }
    }
}
